import React, { Component } from 'react'
import { connect } from 'react-redux'

 class KetQua extends Component {
  render() {
    return (
      <div className=' '>
        <button onClick={()=>{this.props.playGame()}} style={{width: 210, height:150, fontSize:40 }} className='btn btn-success'>Play Game</button>
        <div className='d-flex flex-column py-5' >
          <h3 className='text-danger'>Your Chose:{this.props.chosen}</h3>
          <h3 className='text-success'>You win: {this.props.win}</h3>
          <h3 className='text-primary'>Total: {this.props.total} </h3>
        </div>
      </div>
    )
  }
}
let mapStateToProps=(state)=>({
  chosen: state.taiXiuReducer.chosen,
  win: state.taiXiuReducer.win,
  total: state.taiXiuReducer.total
})
let mapDispatchToProps=(dispatch)=>{
  return {
    playGame:()=>{
      dispatch({
        type: "PLAY_GAME",
        
      }) 
    }

  }
}
export default connect(mapStateToProps,mapDispatchToProps)(KetQua)